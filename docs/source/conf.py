# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'CRC 1456 LiveDocs'
copyright = '2023, CRC 1456 Infrastructure for exchange of research data and software'
author = 'Pedro Costa Klein'

# The full version, including alpha/beta/rc tags
release = '0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

extensions = [
    'sphinx_rtd_theme',
    'm2r2',
    'sphinx.ext.mathjax',
    #'myst_parser',
    #'recommonmark',
]

# MathJax configuration

mathjax_config = {
    'TeX': {'equationNumbers': {'autoNumber': 'AMS', 'useLabelIds': True},
            'inlineMath': [['$', '$'], ['\\(', '\\)']]},
}


source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}

myst_enable_extensions = ["dollarmath", "amsmath"]



# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_extra_path = ['../../livedocs/guided-jupyter-tour-optimal-transport', 
                   '../../livedocs/livedocs-granule-cells-analysis',
                   '../../livedocs/b03-qmri-nist',
                   '../../livedocs/c04_helio',
                   '../../livedocs/b02-gibbsian-polar-slice-sampling',]

html_theme_options = {
    'logo_only': True,
    'navigation_depth': 3,
       
}

html_logo = 'assets/index/LiveDocsLogo-text.png'