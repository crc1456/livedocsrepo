Known Issues
============

Problem accessing the servers using Chrome
------------------------------------------

If the CRC binderhub servers are not reachable in your chrome browser in regular tabs (but are accessible over 
incognito tabs), please proceed as the following: go to `chrome://net-internals/#hsts <chrome://net-internals/#hsts>`_ in your Chrome browser and 
enter the **gwdg.de** domain under *Delete domain security* and click on the **Delete** button.

Math expressions within markdown cells are not rendered
------------------------------------------

In the LiveDocs' pages, in this website, the math expressions within text are not rendered correctly. This is a known issue and we are working on it. In the meantime, please refer to the landing pages on the respective LiveDocs gitlab repositories or to the actual instances of LiveDocs.